/* ____________________________________________________

                      FORM FLYOUTS
_____________________________________________________ */
/* SUBSCRIBE FORM */
var subscribeFlyout = document.getElementById("subscribeForm");
var subscribeFlyout_overlay = document.getElementById("subscribe_form_overlay");
var subscribeFlyout_openBtn = $(".js-open-form--subscribe .dynamic-width-button");
var subscribeFlyout_closeBtn = $("#subscribeForm .js-close-form--overlay");

// opens flyout
$(subscribeFlyout_openBtn).on("click", function(el) {
  el.preventDefault();
  openSubscribeFlyout();
});

function openSubscribeFlyout()
{
  $(subscribeFlyout).addClass("animate");
  $(subscribeFlyout_overlay).fadeIn(300).addClass("in");
}

// closes flyout
$(subscribeFlyout_closeBtn).on("click", function() {
  closeSubscribeFlyout();
});

function closeSubscribeFlyout()
{;
  $(subscribeFlyout).removeClass("animate");
  $(subscribeFlyout_overlay).fadeOut(300).removeClass("in");
}

// closes flyout if click is outside of main area
$(subscribeFlyout_overlay).mousedown(function(e)
{
  var clicked = $(e.target); // element clicked
  if (clicked.is(subscribeFlyout) || clicked.parents().is(subscribeFlyout)) {
    //click happened within menu, do nothing
    return;
  } else {
    //click was outside menu, so close it
   closeSubscribeFlyout();
   }
});

/* CONTACT US FORM */
var contactUsFlyout = document.getElementById("contactUsForm");
var contactUsFlyout_overlay = document.getElementById("contact_us_form_overlay");
var contactUsFlyout_openBtn = $(".js-open-form--contactUs .dynamic-width-button");
var contactUsFlyout_closeBtn = $("#contactUsForm .js-close-form--overlay");

// opens flyout
$(contactUsFlyout_openBtn).on("click", function(el) {
  el.preventDefault();
  openContactUsFlyout();
});

function openContactUsFlyout()
{
  $(contactUsFlyout).addClass("animate");
  $(contactUsFlyout_overlay).fadeIn(300).addClass("in");
}

// closes flyout
$(contactUsFlyout_closeBtn).on("click", function() {
  closeContactUsFlyout();
});

function closeContactUsFlyout()
{;
  $(contactUsFlyout).removeClass("animate");
  $(contactUsFlyout_overlay).fadeOut(300).removeClass("in");
}

// closes flyout if click is outside of main area
$(contactUsFlyout_overlay).mousedown(function(e)
{
  var clicked = $(e.target); // element clicked
  if (clicked.is(contactUsFlyout) || clicked.parents().is(contactUsFlyout)) {
    //click happened within menu, do nothing
    return;
  } else {
    //click was outside menu, so close it
   closeContactUsFlyout();
   }
});


/* ____________________________________________________

                      STYLES
_____________________________________________________ */
var tile = $(".selection-grid .tile");

$(tile).on("click", function() {
  let myTile = $(this);
  if ( !(myTile).hasClass('is-selected') ) {
    $(".tile").removeClass('is-selected');
    $(myTile).addClass('is-selected');
  }
});


/* _____________________________________________

               FORM VALIDATION
______________________________________________ */
function validateContactUsForm() {
  return CONTACT_US.validateForm();
}

function validateSubscribeForm() {
  return SUBSCRIBE.validateForm();
}

;CONTACT_US = {

  validateForm: function() {
    var isValid       = false;
    var allCheckboxes = [];
    var allFormFields = [];

    // find all input and select dropdown fields
    $("#contactUsForm input, #contactUsForm select").each(function() {
      var field = $(this);
      if (field.attr('aria-required') == 'true') {
        // push fields into arr
        allFormFields.push(field);
      }
    });

    var myCheckboxes = [];
    // find all checkboxes in form
    $("#contactUsForm input[type='checkbox']").each(function() {
      var cbInput = $(this);
      // push checkboxes into arr
      myCheckboxes.push(cbInput);
    });

    var checkboxesChecked = [];
    $("#contactUsForm input[type='checkbox']:checked").each(function() {
      if ( $(this).attr('aria-required') == 'true' ) {
        var cbInput = $(this);
        checkboxesChecked.push(cbInput);
      }
    });

    function isChecked(element, index, array) {
      return element.checked != false;
    }

    function hasAValue(element, index, array) {
      return element.attr('aria-required') != 'true' || element.attr('aria-required') == 'true' && element.val() != "" && element.val() != null;
    }

    if ( myCheckboxes.length > 0 && myCheckboxes.length == checkboxesChecked.length && myCheckboxes.every(hasAValue) && allFormFields.every(hasAValue)
      || myCheckboxes.length <= 0 && allFormFields.every(hasAValue) ) {
        console.log('is valid');
      // submit form
      isValid = true;
    } else {
      $("#contactUsForm .js-form-field-error-message").css("display", "block");
      myCheckboxes.forEach(function(element) {
        if (element.attr('aria-required') == 'true') {
          if (element.is(':checked') != false) {
            element.next().css('color', '#121212');
          }
          else {
            element.next().css('color', '#ed002a');
          }
        }
      });

      // loop through to find empty elements and add border color
      allFormFields.forEach(function(element) {
        if (element.attr('aria-required') != 'true' || element.attr('aria-required') == 'true' && element.val() != "" && element.val() != null)  {
          element.css("border", "1px solid #b1b3b3");
        } else {
          element.css("border", "1px solid #ed002a");
        }
      });

      // do not submit form
      isValid = false;
    }

    if (isValid === true) {
      return true;
    } else {
      return false;
    }
  }
}

;SUBSCRIBE = {

  validateForm: function() {
    var isValid       = false;
    var allCheckboxes = [];
    var allFormFields = [];

    // find all input and select dropdown fields
    $("#subscribeForm input, #subscribeForm select").each(function() {
      var field = $(this);
      if (field.attr('aria-required') == 'true') {
        // push fields into arr
        allFormFields.push(field);
      }
    });

    var myCheckboxes = [];
    // find all checkboxes in form
    $("#subscribeForm input[type='checkbox']").each(function() {
      var cbInput = $(this);
      // push checkboxes into arr
      myCheckboxes.push(cbInput);
    });

    var checkboxesChecked = [];
    $("#subscribeForm input[type='checkbox']:checked").each(function() {
      if ( $(this).attr('aria-required') == 'true' ) {
        var cbInput = $(this);
        checkboxesChecked.push(cbInput);
      }
    });

    function isChecked(element, index, array) {
      return element.checked != false;
    }

    function hasAValue(element, index, array) {
      return element.attr('aria-required') != 'true' || element.attr('aria-required') == 'true' && element.val() != "" && element.val() != null;
    }

    if ( myCheckboxes.length > 0 && myCheckboxes.length == checkboxesChecked.length && myCheckboxes.every(hasAValue) && allFormFields.every(hasAValue)
      || myCheckboxes.length <= 0 && allFormFields.every(hasAValue) ) {

      // submit form
      isValid = true;
    } else {
      $("#subscribeForm .js-form-field-error-message").css("display", "block");
      myCheckboxes.forEach(function(element) {
        if (element.attr('aria-required') == 'true') {
          if (element.is(':checked') != false) {
            element.next().css('color', '#121212');
          }
          else {
            element.next().css('color', '#ed002a');
          }
        }
      });

      // loop through to find empty elements and add border color
      allFormFields.forEach(function(element) {
        if (element.attr('aria-required') != 'true' || element.attr('aria-required') == 'true' && element.val() != "" && element.val() != null)  {
          element.css("border", "1px solid #b1b3b3");
        } else {
          element.css("border", "1px solid #ed002a");
        }
      });

      // do not submit form
      isValid = false;
    }

    if (isValid === true) {
      return true;
    } else {
      return false;
    }
  }
}
