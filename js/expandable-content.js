$(document).ready(function() {

  $(window).resize(function(){
    location.reload();
  });

  addLoadEvent(function() {
    // find this row of items
    var content = $('.js-expandable-content-row');

    // keep the functions inside this item
    content.find('.expandable-content-item__preview').on("click", function() {
      // find the closest expandable item
      var thisContent = $(this).closest('.expandable-content-item');
      // stay within this instance of the component
      var thisExpandableContent = $(this).closest('.comp__expandable-content');

      // functionality for desktop - laptop - tablet landscape
      if ($(window).width() > 768) {
        if (!thisContent.hasClass('is-expanded')) {
          // if container is currently closed
          // find row offset & set expandable container offset to match
          var contentLeftOffset = $('.js-expandable-content-row').offset().left;
          $('.expandable-content--expanded').offset({left: contentLeftOffset});
          // close any other containers that might be open
          content.not(thisContent).removeClass('is-expanded'); // ??? do we need this??
          // close sibling containers and remove active styles
          thisContent.siblings().removeClass('is-expanded').removeClass('has-active-styles');
          // open this item and add active styles
          thisContent.addClass('is-expanded').addClass('has-active-styles');

          return false;

        } else if (thisContent.hasClass('is-expanded')) {
          // if container is currently open
          // close this container & remove active styles
          thisContent.removeClass('is-expanded').removeClass('has-active-styles');
        }
      }

      // functionality for tablet portrait - mobile
      if ($(window).width() <= 768) {
        // if container is currently closed
        if (!thisContent.hasClass('is-expanded')) {
          // get background color from component level and apply it to overlay container
          let myBgColor = $(thisExpandableContent).css('background-color');
          $('.expandable-content--expanded .expanded-content-container').css('background-color', myBgColor).css('z-index', '3000');
          // open this item
          thisContent.addClass('is-expanded');
          // make sure body doesn't scroll behind overlay
          $("body").css("overflow-y", "hidden");

          return false;
        }
      }
      // put return false inside the if statement so that it only returns false if expander is in use
      // otherwise it prevents non-expander links from working
    });


    // mobile close button
    content.find('.expanded-content__mobile-close').on("click", function() {
      // find the open item
      var thisContent = $(this).closest('.expandable-content-item');
      // close the overlay
      thisContent.removeClass('is-expanded');
      // return body to normal
      $("body").css("overflow-y", "initial");
      return false;
    });

    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[id=' + this.hash.slice(1) +']');
        if (target.length) {
            // if on tablet or mobile, jump to top instead of scrolling
            if ($(window).width() <= 767) {
              $('html, body').animate({
                scrollTop: target.offset().top
              }, 0);
          }
          else if ($(window).width() > 767) {
            $('html, body').animate({
                scrollTop: target.offset().top
              }, 700);
          }
          }
        }
    });

    /* This must come after the click functions are defnined above or the click will have nothing to do. */
    if (window.location.hash) {
      var $hash = window.location.hash;
      $('a[href="' + $hash + '"]').parents('.expandable-content-item__preview').click();
    }
  });
})
