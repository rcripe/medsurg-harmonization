$(document).ready(function () {
    $('.js-expander-trigger h6').click(function (e) {
        var windowWidth = $(window).width();
        if (windowWidth < 769) {
            $(this).parent().find('.js-submenu').slideToggle('fast');
            $(this).children().children('i').toggleClass('is-active');
        }
        e.preventDefault();
    });
});

window.addEventListener('resize', function () {
    var windowWidth = $(window).width();
    if (windowWidth >= 769) {
        $('.js-expander-trigger').find('.js-submenu').show();
    } else if (windowWidth < 769) {
        $('.js-expander-trigger').find('.js-submenu').hide();
    }
});
