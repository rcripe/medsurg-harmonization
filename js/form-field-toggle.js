var formRadioToggleA = document.getElementById("formRadioToggleA");
var formRadioToggleB = document.getElementById("formRadioToggleB");
var formToggleFieldA = document.getElementById("formToggleFieldA");
var formToggleFieldB = document.getElementById("formToggleFieldB");

formRadioToggleA.addEventListener("click", function() {
  $(formToggleFieldA).fadeIn(300);
  formToggleFieldB.style.display = "none";
});

formRadioToggleB.addEventListener("click", function() {
  $(formToggleFieldB).fadeIn(300);
  formToggleFieldA.style.display = "none";
});

$(document).ready(function() {
  $(formToggleFieldB).hide();
});
