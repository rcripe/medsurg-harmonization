$(function() {

  window.openVideoInPlace = openVideoInPlace;
  window.buildS7videoPlayer = buildS7videoPlayer;

  $('.js-close-inplacevideo').click(function (event) {
      event.preventDefault();
      $('#videoplayervideo').get(0).pause();
      $('#videoplayervideo').prop('src', false);
      $('#videoplayervideo').get(0).load();
      var videoBox = $('#videoplayervideo').parents('.js-inplacevideo');
      $(videoBox).find('.js-inplacevideo-trigger').show();
      $(videoBox).find('.inPlaceVideoPlayerContainer').empty();
      $(videoBox).find('.inPlaceVideoPlayerContainer').hide();
      $(this).hide();
  });

  /* Open video in place rather than in lightbox */
  function openVideoInPlace(el, source) {
    var videoBox = $(el).closest(".panel,[class^='comp__']").find('.js-inplacevideo');
    /* check if the video player is already open, and close it if it is.
       this is possible because of multiple triggers to open the video,
       such as the View Now button and the play video symbol button. */
    if ($("#videoplayervideo").length) {
      $('#videoplayervideo').get(0).pause();
      $('#videoplayervideo').prop('src', false);
      $('#videoplayervideo').get(0).load();
      var vidBox = $('#videoplayervideo').parents('.js-inplacevideo');
      $(vidBox).find('.js-inplacevideo-trigger').show();
      $(vidBox).find('.inPlaceVideoPlayerContainer').empty();
      $(vidBox).find('.inPlaceVideoPlayerContainer').hide();
      $(vidBox).find('.js-close-inplacevideo').hide();
    }

    var width = $(videoBox).width();
    //var ratio = 1.77777777777778;
    var ratio = (1920/1080);
    //var height = width / ratio;
    var height = $(videoBox).height();

    $(videoBox).find('.js-inplacevideo-trigger').hide();
    $(videoBox).find('.js-close-inplacevideo').css("display", "flex");
    $(videoBox).find('.inPlaceVideoPlayerContainer').show();

    buildS7videoPlayer($(videoBox).find('.inPlaceVideoPlayerContainer'), width, height, source);
  }

  /* Build Scene7 Videoplayer */
  function buildS7videoPlayer(container, width, height, source) {
      $(container).append($('' +
          '<div class="videoplayer">' +
          '   <video id="videoplayervideo" class="html5video in-place-video" controls width="' + width + 'px" height="' + height + 'px" autoplay preload="none" style="width:100%">' +
          '       <source src="' + source + '" type="video/mp4"/>' +
          '       <p class="vjs-no-js">' +
          '           To view this video please enable JavaScript, and consider upgrading to a web browser that' +
          '           <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>' +
          '       </p>' +
          '   </video>' +
          '</div>')
      );
  }
});
