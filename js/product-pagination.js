// PRODUCT CARD PAGINATION //
const carousel = document.querySelector(".comp__content-cards .related-pages-slider");
const productCard = document.getElementsByClassName("js-content-card");
const previousBtn = document.querySelector(".comp__content-cards .js-previous");
const nextBtn = document.querySelector(".comp__content-cards .js-next");

// let cardArray = [];
let list = [];
let pageList = [];
let currentPage = 1;
let numberPerPage;
let numberOfPages = 0;

let productCards = document.querySelector(".content-cards");
// find out if 3 or 4 col
if (productCards.classList.contains("four-col")) {
  numberPerPage = 4;
}

if (productCards.classList.contains("three-col")) {
  numberPerPage = 3;
}

function makeList() {
  for (let i=0; i<productCard.length; i++) {
    list.push(productCard[i]);
  }
  numberOfPages = getNumberOfPages();
  // writeNumberOfPages();
}

function getNumberOfPages() {
  return Math.ceil(list.length / numberPerPage);
}

// function writeNumberOfPages() {
//   totalSets.innerHTML = numberOfPages;
// }

// function writeCurrentPage() {
//   currentSet.innerHTML = currentPage;
// }

function nextPage() {
  currentPage += 1;
  loadList();
}

function previousPage() {
  currentPage -= 1;
  loadList();
}

function loadList() {
  var begin = ((currentPage - 1) * numberPerPage);
  var end = begin + numberPerPage;
  pageList = list.slice(begin, end);
  drawList();
  check();
}

function drawList() {
  $(".js-content-card").hide();

  for (r=0; r<pageList.length; r++) {
    // console.log(pageList[r]);
    pageList[r].style.display = "flex"; //do css class with media queries instead?
  }
  // writeCurrentPage();
}

function check() {
  document.getElementById("next").disabled = currentPage == numberOfPages ? true : false;
  document.getElementById("previous").disabled = currentPage == 1 ? true : false;
}

function load() {
  makeList();
  loadList();
}

previousBtn.addEventListener("click", function() {
  // console.log("prev button click");
  previousPage();
});

nextBtn.addEventListener("click", function() {
  // console.log("next button click");
  nextPage();
});

window.onload = load;
