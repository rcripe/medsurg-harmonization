$(document).ready(function() {

  let relatedContentSlider = $(".comp__related-content-slider");

  if ( relatedContentSlider.length ) {

    $(relatedContentSlider).each(function() {
      const this_slider = $(this);
      const this_item = this_slider.find(".js-slider-deck > .js-slider-card");
      const this_forwardButton = this_slider.find(".js-slide-forward");
      const this_backButton = this_slider.find(".js-slide-back");
      const this_itemCount = this_slider.find(".js-slider-deck > .js-slider-card").length;
      const this_sliderWrapper = this_slider.find(".slider-wrapper");
      const this_container = $(this_sliderWrapper).width();
      const this_cardWidth = $(this_item).width();

      let this_cardLengthL = '+=' + (this_cardWidth + 20) + 'px';
      let this_cardLengthR = '-=' + (this_cardWidth + 20) + 'px';
      let this_index = 0;
      let this_numVisible = (this_container / this_cardWidth);
      let this_endIndex = ( this_item.length - this_numVisible );

      // disable arrows initially
      $(this_backButton).prop('disabled', true);
      $(this_forwardButton).prop('disabled', true);

      // show right arrow if item count exceeds num visible
      if (this_itemCount > this_numVisible) {
        $(this_forwardButton).prop('disabled', false);
      }

      // right arrow click func
      $(this_forwardButton).click(function() {
        if (this_index < this_endIndex) {
          $(this).prop('disabled', false);
          this_index++;
          this_item.animate({'left': this_cardLengthR });
        }
        if (this_index >= 1) {
          $(this_backButton).prop('disabled', false);
        }
        if (this_index >= this_endIndex) {
          $(this).prop('disabled', true);
        }
      });

      // left arrow click func
      $(this_backButton).click(function() {
        if (this_index > 0) {
          $(this).prop('disabled', false);
          this_index--;
          this_item.animate({'left': this_cardLengthL });
        }
        if (this_index < this_endIndex) {
          $(this_forwardButton).prop('disabled', false);
        }
        if (this_index <= 0) {
          $(this).prop('disabled', true);
        }
      });
    });
  }
});
