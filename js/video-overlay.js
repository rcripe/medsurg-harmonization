$(function() {
  $('.js-open-video-overlay').on('click', function(e) {
    e.preventDefault();
    $('#videoOverlayContainer').fadeIn(200);
    $('.js-video-modal').fadeIn();
    $('.js-video-modal-overlay').addClass('in');
    $('.js-video-modal-overlay').css('display', 'block');
    $('body').addClass('modal-open');
  });

  $('.js-close-video-overlay').click(function() {
    $('#videoOverlayContainer').fadeOut(200);
      $('.js-video-modal').fadeOut();
      $('.js-video-modal-overlay').css('display', 'none');
      $('body').removeClass('modal-open');
  });
});
